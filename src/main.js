import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import router from './router'
import axios from 'axios';

import VueApollo from 'vue-apollo'
import ApolloClient from 'apollo-boost'
// import { InMemoryCache } from 'apollo-cache-inmemory'
import store from './store'

Vue.config.productionTip = false

const backendUrl = 'https://backend-springapi.herokuapp.com'

Vue.prototype.$axios = axios;
Vue.prototype.$axios.interceptors.request.use(
  config => {
    if (store.getters.isLoggedIn) config.headers['X-Token-Auth'] = localStorage.getItem('token');
    config.baseURL = backendUrl;
    return config;
  }
);
Vue.prototype.$axios.interceptors.response.use(function (response) {
  return Promise.resolve(response.data)
}, function (error) {
  const originalRequest = error.config;
  // console.log(originalRequest)
  if (error.response.status === 401 &&
    originalRequest.url === `${backendUrl}/v1/auth/login`) {
    store.dispatch('logout')
      .then(() => {
        if (router.currentRoute.name !== 'login') {
          router.push('/login')
        }
      });
    return Promise.reject(error.response.data.status)
  }
  if (error.response.status === 401 &&
    !originalRequest._retry) {
    originalRequest._retry = true;
    return axios({
      url: `${backendUrl}/v1/auth/token?refreshToken=` + localStorage.getItem('refresh_token'),
      method: 'GET',
    })
      .then(res => {
        if (res.meta.code === 200) {
          const token = res.data.accessToken;
          localStorage.setItem('token', token);
          axios.defaults.headers.common['X-Token-Auth'] = token;
          return axios(originalRequest);
        }
      })
      .catch(() => {
        store.dispatch('logout')
          .then(() => {
            if (router.currentRoute.name !== 'login') {
              router.push('/login')
            }
          });
      })
  }
  return Promise.reject(error ? error : { response: "Something went wrong!" })
});

// Create the apollo client
const apolloClient = new ApolloClient({
  uri: `https://backend-springapi.herokuapp.com/graphqls`,
  request: operation => {
    operation.setContext({
      headers: {
        'X-Token-Auth': localStorage.getItem('token')
      },
    });
  },
  onError: error => {
    if (error.networkError && error.networkError.statusCode === 401) {
      store.dispatch('logout')
          .then(() => {
            if (router.currentRoute.name !== 'Login') {
              router.push({name: 'Login'})
            }
          });
    }
  }
  // cache: new InMemoryCache(),
})

Vue.use(VueApollo)
const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
})

new Vue({
  apolloProvider: apolloProvider,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
