const authMixin = {
	data() {
		return {
			loginData: ''
		}
	},
	computed: {
		isLoggedIn: function () {
			return !!(this.loginData && this.loginData.accessToken)
		}
	},
	watch: {},
	methods: {
		checkLogin: function() {
			if (!this.$store.user) {
				this.loginData = JSON.parse(localStorage.getItem('user'))
				this.$store.commit('auth_success', this.loginData)
			}
		}
	},
	mounted() {
		this.checkLogin()
	}
}

export default authMixin