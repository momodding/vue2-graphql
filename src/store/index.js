import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: {},
    status: '',
    respMessage: '',
  },
  mutations: {
    auth_success: (state, user) => {
      state.status = 'success'
      state.user = user
      state.respMessage = ''
    },
    logout: (state) => {
      state.status = ''
      state.user = {}
      state.respMessage = ''
    },
  },
  actions: {
    logout({ commit }) {
      return new Promise((resolve) => {
        commit('logout')
        localStorage.removeItem('token')
        localStorage.removeItem('user')
        delete this._vm.$axios.defaults.headers.common['X-Token-Auth']
        resolve();
      })
    },
  },
  getters: {},
  modules: {
  }
})
